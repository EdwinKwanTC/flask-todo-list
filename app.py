from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

items = []

class Item(Resource):
    def get(self, name):
        return {item['name']: item['price'] for item in items if item['name'] == name}


class ItemList(Resource):
    def get(self):
        return items

    def post(self):
        data = request.get_json()
        name = data['name']
        filter_item = filter(lambda x: x['name'] == name, items)

        # if item was not found in the list
        if len(list(filter_item)) > 0:
            return {'msg': 'item exit'}, 500

        item = {"name": data['name'], "price":data['price']}
        items.append(item)
        return {'data': data}

api.add_resource(ItemList, '/item')
api.add_resource(Item, '/item/<string:name>')

if __name__ == '__main__':
    app.run(debug=True)